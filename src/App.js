import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import ArtistsList from "./containers/ArtistsList/ArtistsList";
import AlbumsList from "./containers/AlbumsList/AlbumsList";
import TracksOfAlbum from "./containers/TracksOfAlbum/TracksOfAlbum";
import Layout from "./containers/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TracksHistory from "./containers/TracksHistory/TracksHistory";
import AddNewArtist from "./containers/AddForms/AddNewArtist/AddNewArtist";
import AddNewAlbum from "./containers/AddForms/AddNewAlbum/AddNewAlbum";
import AddNewTrack from "./containers/AddForms/AddNewTrack/AddNewTrack";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={ArtistsList} />
          <Route path="/artists" exact component={ArtistsList} />
          <Route path="/artists/admin" exact component={ArtistsList} />
          <Route path="/my-artists/:id" exact component={ArtistsList} />
          <Route path="/artists/add-new-artist" exact component={AddNewArtist} />
          <Route path="/albums" component={AlbumsList} />
          <Route path="/albums/add-new-album" exact component={AddNewAlbum} />
          <Route path="/my-albums/:id" exact component={AlbumsList} />
          <Route path="/tracks" exact component={TracksOfAlbum} />
          <Route path="/tracks/add-new-track" exact component={AddNewTrack} />
          <Route path="/history" exact component={TracksHistory} />
          <Route path="/register" exact component={Register} />
          <Route path="/login" exact component={Login} />
          <Route render={() => <h1 style={{textAlign: 'center'}}>Page not found</h1>} />
        </Switch>
      </Layout>
    );
  }
}

export default App;
