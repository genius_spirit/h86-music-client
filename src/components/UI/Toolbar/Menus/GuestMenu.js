import React from "react";
import { Icon, Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

const linkStyle = {
  fontSize: "16px"
};

const GuestMenu = () => {
  return (
    <Menu.Menu position="right">
      <Menu.Item>
        <Link to="/register" style={linkStyle}>
          <Icon name="add user" style={{marginRight: '10px'}}/>Sign Up
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/login" style={linkStyle}>
          <Icon name="user" style={linkStyle}/> Log In
        </Link>
      </Menu.Item>
    </Menu.Menu>
  );
};

export default GuestMenu;
