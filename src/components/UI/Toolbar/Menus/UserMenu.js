import React from "react";
import { Dropdown, Icon, Menu } from "semantic-ui-react";
import { NavLink } from "react-router-dom";

const linkStyle = {
  fontSize: "16px",
  color: '#333'
};
const UserMenu = ({ user, logout }) => {
  return (
    <Menu.Menu position="right">
      {user.role === 'user' ?
        <Dropdown item text={'Hello, ' +  user.username} style={linkStyle}>
          <Dropdown.Menu>
            <Dropdown.Item>
              <NavLink to={"/my-artists/" + user._id} style={linkStyle}>
                My artists
              </NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink to={"/my-albums/" + user._id} style={linkStyle}>
                My albums
              </NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink to={"/my-tracks/" + user._id} style={linkStyle}>
                My tracks
              </NavLink>
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown> :
        <Menu.Item>
          <Icon name="wifi" style={{fontSize: "18px"}}/>
          {'Hello, ' +  user.username}
        </Menu.Item>
      }

      <Menu.Item onClick={logout} >
        <Icon name="log out" style={{fontSize: "18px"}}/> Logout
      </Menu.Item>
    </Menu.Menu>
  );
};

export default UserMenu;
