import React, { Component } from "react";
import { Dropdown, Menu } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import logo from "../../../assets/images/logo.png";
import UserMenu from "./Menus/UserMenu";
import GuestMenu from "./Menus/GuestMenu";

const linkStyle = {
  fontSize: "16px",
  color: "#333"
};

class Toolbar extends Component {
  render() {
    return (
      <Menu
        style={{
          marginBottom: "30px",
          borderTopLeftRadius: "0",
          borderTopRightRadius: "0"
        }}
      >
        <Menu.Item>
          <NavLink to="/">
            <img src={logo} alt="logo" width={25} height={25} />
          </NavLink>
        </Menu.Item>

        <NavLink
          className="item"
          to="/artists"
          exact
          style={linkStyle}
          activeClassName="selected"
        >
          Artists
        </NavLink>

        <NavLink
          className="item"
          to="/albums"
          exact
          style={linkStyle}
          activeClassName="selected"
        >
          Albums
        </NavLink>

        {(!this.props.user) || (this.props.user && this.props.user.role === 'user') ?
          <NavLink
            className="item"
            to="/history"
            exact
            style={linkStyle}
            activeClassName="selected"
          >
            Tracks history
          </NavLink> : null }

        {this.props.user && this.props.user.role === 'user' ? (
          <Dropdown item text="Add" style={linkStyle}>
            <Dropdown.Menu>
              <Dropdown.Item>
                <NavLink to="/artists/add-new-artist" style={linkStyle}>
                  Add new artist
                </NavLink>
              </Dropdown.Item>
              <Dropdown.Item>
                <NavLink to="/albums/add-new-album" style={linkStyle}>
                  Add new album
                </NavLink>
              </Dropdown.Item>
              <Dropdown.Item>
                <NavLink to="/tracks/add-new-track" style={linkStyle}>
                  Add new track
                </NavLink>
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        ) : null}
        {this.props.user ? (
          <UserMenu user={this.props.user} logout={this.props.logout} />
        ) : (
          <GuestMenu />
        )}
      </Menu>
    );
  }
}

export default Toolbar;
