import React from "react";
import PropTypes from 'prop-types';
import { Card } from "semantic-ui-react";
import config from "../../config";

const CardItem = props => {

  const imageSrc = config.apiUrl + "/uploads/";

  return (
    <Card
      raised={props.raised}
      image={imageSrc + props.image}
      header={props.header}
      description={props.description}
      link={props.link}
      onClick={props.onClick}
      extra={!props.publish && 'Not published!'}
    />
  );
};

CardItem.propTypes = {
  raised: PropTypes.bool,
  image: PropTypes.string.isRequired,
  header: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  link: PropTypes.bool,
  onClick: PropTypes.func
};

export default CardItem;
