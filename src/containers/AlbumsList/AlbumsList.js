import React, { Component } from "react";
import { connect } from "react-redux";
import { Card } from "semantic-ui-react";
import { getTracksOfAlbum } from "../../store/actions/tracks";
import CardItem from "../../components/CardItem/CardItem";
import {
  getAlbumsList,
  getAlbumsListForAdmin,
  getMyAlbumsList
} from "../../store/actions/albums";

class AlbumsList extends Component {

  componentDidMount() {
    if (this.props.location.pathname === `/my-albums/${this.props.match.params.id}` ) {
      this.props.onGetMyAlbumsList(this.props.match.params.id);
    }

    if (this.props.location.pathname === '/albums') {
      if (this.props.user && this.props.user.role === 'admin') {
        this.props.getAlbumsListForAdmin();
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if ( nextProps.location.pathname === `/my-albums/${nextProps.match.params.id}` ) {
      if (nextProps.location.key !== this.props.location.key) {
        this.props.onGetMyAlbumsList(nextProps.match.params.id);
      }
    }
    if ( nextProps.location.key !== this.props.location.key && nextProps.location.search === '') {
      this.props.onGetAlbumsList();
    }
    if ( nextProps.location.key !== this.props.location.key && !this.props.user) {
      this.props.onGetAlbumsList();
    }
  }

  render() {
    return (
      <React.Fragment>
        <Card.Group centered stackable>
          {this.props.albums &&
            this.props.albums.map(item => (
              <CardItem
                key={item._id}
                raised={true}
                image={item.photo}
                header={item.name}
                description={item.year + " year"}
                onClick={() => this.props.onGetTracksOfAlbum(item._id)}
                link={true}
                publish={item.publish}
              />
            ))}
        </Card.Group>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    albums: state.albums.albums,
    user: state.users.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetAlbumsList: () => dispatch(getAlbumsList()),
    getAlbumsListForAdmin: () => dispatch(getAlbumsListForAdmin()),
    onGetTracksOfAlbum: id => dispatch(getTracksOfAlbum(id)),
    onGetMyAlbumsList: id => dispatch(getMyAlbumsList(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumsList);
