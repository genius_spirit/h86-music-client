import React, { Component } from "react";
import { connect } from "react-redux";
import { getAlbumsList } from "../../../store/actions/albums";
import { Button, Col, Form, FormGroup } from "react-bootstrap";
import FormElement from "../../../components/Form/FormElement/FormElement";
import { addNewTrack } from "../../../store/actions/tracks";

class AddNewTrack extends Component {

  state = {
    position: '',
    name: '',
    album: '',
    duration: '',
    file: '',
    youtube: '',
    userId: this.props.user._id
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  componentDidMount() {
    this.props.getAlbumsList();
  }

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>

        <FormElement
          propertyName="album"
          title="Album of track"
          type="select"
          options={this.props.albums && this.props.albums}
          value={this.state.album}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="position"
          title="Number of position"
          type="number"
          value={this.state.position}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="name"
          title="Title of track"
          type="text"
          value={this.state.name}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="duration"
          title="Duration"
          type="text"
          value={this.state.duration}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="youtube"
          title="Youtube link (example: 1OFaaU1CiqY) (not required)"
          type="text"
          value={this.state.youtube}
          changeHandler={this.inputChangeHandler}
        />

        <FormElement
          propertyName="file"
          title="MP3 file (not required)"
          type="file"
          changeHandler={this.fileChangeHandler}
        />

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    albums: state.albums.albums
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getAlbumsList: () => dispatch(getAlbumsList()),
    onSubmit: trackData => dispatch(addNewTrack(trackData))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewTrack);