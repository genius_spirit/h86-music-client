import React, { Component } from "react";
import { Button, Col, Form, FormGroup } from "react-bootstrap";
import FormElement from "../../../components/Form/FormElement/FormElement";
import { fetchArtistsList } from "../../../store/actions/artists";
import { connect } from "react-redux";
import { addNewAlbum } from "../../../store/actions/albums";

class AddNewAlbum extends Component {

  state = {
    name: '',
    year: '',
    about: '',
    photo: '',
    author: '',
    userId: this.props.user._id
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  componentDidMount() {
    this.props.fetchArtistsList();
  }

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>

        <FormElement
          propertyName="author"
          title="Author of album"
          type="select"
          options={this.props.artists && this.props.artists}
          value={this.state.author}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="name"
          title="Name of album"
          type="text"
          value={this.state.name}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="year"
          title="Year of album"
          type="number"
          value={this.state.year}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="about"
          title="About album"
          type="textarea"
          value={this.state.about}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="photo"
          title="Photo of album"
          type="file"
          changeHandler={this.fileChangeHandler}
        />

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    artists: state.artists.artists
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchArtistsList: () => dispatch(fetchArtistsList()),
    onSubmit: albumData => dispatch(addNewAlbum(albumData))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewAlbum);