import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../../../components/Form/FormElement/FormElement";
import { connect } from "react-redux";
import { addNewArtist } from "../../../store/actions/artists";

class AddNewArtist extends Component {
  state = {
    name: '',
    info: '',
    photo: '',
    userId: this.props.user._id
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>

        <FormElement
          propertyName="name"
          title="Name of Artist"
          type="text"
          value={this.state.name}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="info"
          title="About of Artist"
          type="textarea"
          value={this.state.info}
          changeHandler={this.inputChangeHandler}
          required
        />

        <FormElement
          propertyName="photo"
          title="Photo of artist (1024 x 768px)"
          type="file"
          changeHandler={this.fileChangeHandler}
        />

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onSubmit: artistData => dispatch(addNewArtist(artistData))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewArtist);
