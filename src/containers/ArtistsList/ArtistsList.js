import React, { Component } from "react";
import { connect } from "react-redux";

import { getAlbumsOfArtist } from "../../store/actions/albums";
import {
  fetchArtistsList,
  getArtistsListForAdmin,
  getMyArtistsList
} from "../../store/actions/artists";
import { Card } from "semantic-ui-react";
import CardItem from "../../components/CardItem/CardItem";

class ArtistsList extends Component {
  componentDidMount() {
    if (this.props.user && this.props.user.role === "admin") {
      this.props.getArtistsListForAdmin();
    } else {
      this.props.fetchArtistsList();
    }

    if ( this.props.location.pathname === `/my-artists/${this.props.match.params.id}` ) {
      this.props.getMyArtistsList(this.props.match.params.id);
    }
  }

  componentWillReceiveProps(nextProps) {
    if ( nextProps.location.pathname === `/my-artists/${nextProps.match.params.id}` ) {
      if (nextProps.location.key !== this.props.location.key) {
        this.props.getMyArtistsList(nextProps.match.params.id);
      }
    }
    if ( nextProps.location.key !== this.props.location.key && !this.props.user ) {
      this.props.fetchArtistsList();
    }
  }

  render() {
    return (
      <Card.Group centered stackable>
        {this.props.artists.map(item => (
          <CardItem
            key={item._id}
            raised={true}
            image={item.photo}
            header={item.name}
            description={item.info}
            onClick={() => this.props.getAlbumsOfArtist(item._id)}
            link={true}
            style={{ fontSize: "12px" }}
            publish={item.publish}
          />
        ))}
      </Card.Group>
    );
  }
}

const mapStateToProps = state => {
  return {
    artists: state.artists.artists,
    user: state.users.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchArtistsList: () => dispatch(fetchArtistsList()),
    getArtistsListForAdmin: () => dispatch(getArtistsListForAdmin()),
    getAlbumsOfArtist: id => dispatch(getAlbumsOfArtist(id)),
    getMyArtistsList: id => dispatch(getMyArtistsList(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArtistsList);
