import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { Table } from "semantic-ui-react";
import { getTracksHistory } from "../../store/actions/tracksHistory";

class TracksHistory extends Component {

  componentDidMount() {
    if (this.props.user) this.props.getTracksHistory(this.props.user._id);
  }

  render() {
    let table = (
      <Table celled striped>
        <Table.Body>
          {this.props.history && this.props.history.map(item => (
            <Table.Row key={item._id}>
              <Table.Cell>
                {item.track.album.author.name +
                  " - " +
                  item.track.name +
                  " ( " +
                  item.track.album.name +
                  " ) "}
              </Table.Cell>
              <Table.Cell collapsing>{item.datetime}</Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    );

    return (
      <React.Fragment>
        {this.props.user ? table : <Redirect to="/login" />}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    history: state.tracksHistory.history
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getTracksHistory: id => dispatch(getTracksHistory(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TracksHistory);
