import React, { Component } from "react";
import { connect } from "react-redux";
import { Divider, Header, Icon, Segment } from "semantic-ui-react";

import { postTrackToHistory } from "../../store/actions/tracksHistory";
import { getTracksOfAlbum } from "../../store/actions/tracks";
import Modal from "../../components/UI/Modal/Modal";
import ModalVideo from "react-modal-video";
import '../../../node_modules/react-modal-video/css/modal-video.min.css';

class TracksOfAlbum extends Component {

  state = {
    show: false,
    youtube: ''
  };


  getQueryString = () => {
    const params = new URLSearchParams(this.props.location.search);
    return params.get('album');
  };

  clickOnTrack = (id, link) => {
    if (this.props.user) {
      this.props.postTrackToHistory(id, this.props.user.token);
      this.setState({show: true, youtube: link});
    }
  };

  closeModal = () => {
    this.setState({show: false})
  };

  componentDidMount() {
    this.props.onGetTracksOfAlbum(this.getQueryString());
  }

  render() {
    return (
      <React.Fragment>
        <Header as='h1'>Tracks</Header>
        <Divider/>
        {this.props.tracks.map(item => (
          <Segment key={item._id} style={{cursor: 'pointer'}}
                   onClick={() => this.clickOnTrack(item._id, item.youtube)}
                   >
            {item.position + '. - ' + item.name}
            {item.youtube
              ? <Icon name="youtube"
                      color="red"
                      size="large"
                      style={{marginLeft: '20px'}}
                      link
                      />
              : null}
          </Segment>
        ))}
        <Modal show={this.state.show} closed={this.closeModal}>
          <ModalVideo channel='youtube'
                      isOpen={this.state.show}
                      videoId={this.state.youtube}
                      />
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    tracks: state.tracks.tracks,
    user: state.users.user,
    album: state.tracks.album
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postTrackToHistory: (track, token) => dispatch(postTrackToHistory(track, token)),
    onGetTracksOfAlbum: (id) => dispatch(getTracksOfAlbum(id))
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(TracksOfAlbum);
