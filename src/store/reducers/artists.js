import {
  ADD_NEW_ARTIST_FAILURE,
  ARTIST_LIST_FAILURE,
  ARTIST_LIST_SUCCESS,
  GET_ARTIST_LIST_FOR_ADMIN_FAILURE,
  GET_ARTIST_LIST_FOR_ADMIN_SUCCESS,
  GET_MY_ARTISTS_LIST_FAILURE,
  GET_MY_ARTISTS_LIST_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  artists: [],
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ARTIST_LIST_SUCCESS:
      return { ...state, artists: action.data };
    case ARTIST_LIST_FAILURE:
      return { ...state, error: action.error };
    case ADD_NEW_ARTIST_FAILURE:
      return { ...state, error: action.error };
    case GET_MY_ARTISTS_LIST_SUCCESS:
      return { ...state, artists: action.data };
    case GET_MY_ARTISTS_LIST_FAILURE:
      return { ...state, error: action.error };
    case GET_ARTIST_LIST_FOR_ADMIN_SUCCESS:
      return { ...state, artists: action.data };
    case GET_ARTIST_LIST_FOR_ADMIN_FAILURE:
      return { ...state, error: action.error };
    default:
      return state;
  }
};

export default reducer;
