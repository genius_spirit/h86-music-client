import {
  GET_ALBUM_BY_ID_FAILURE,
  GET_ALBUM_BY_ID_SUCCESS,
  TRACKS_OF_ALBUM_FAILURE,
  TRACKS_OF_ALBUM_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  tracks: [],
  album: [],
  trackError: null,
  albumError: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case TRACKS_OF_ALBUM_SUCCESS:
      return {...state, tracks: action.tracksData};
    case TRACKS_OF_ALBUM_FAILURE:
      return {...state, error: action.trackError};
    case GET_ALBUM_BY_ID_SUCCESS:
      return {...state, album: action.albumData};
    case GET_ALBUM_BY_ID_FAILURE:
      return {...state, albumError: action.error};
    default:
      return state;
  }
};

export default reducer;