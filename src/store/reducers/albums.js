import {
  ADD_NEW_ALBUM_FAILURE,
  ADD_NEW_ALBUM_SUCCESS,
  ALBUMS_LIST_FAILURE,
  ALBUMS_LIST_SUCCESS,
  ALBUMS_OF_ARTIST_FAILURE,
  ALBUMS_OF_ARTIST_SUCCESS, GET_ALBUMS_LIST_FOR_ADMIN_FAILURE,
  GET_ALBUMS_LIST_FOR_ADMIN_SUCCESS,
  GET_MY_ALBUMS_LIST_FAILURE,
  GET_MY_ALBUMS_LIST_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  albums: [],
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ALBUMS_LIST_SUCCESS:
      return {...state, albums: action.data};
    case ALBUMS_LIST_FAILURE:
      return {...state, error: action.error};
    case ALBUMS_OF_ARTIST_SUCCESS:
      return {...state, albums: action.data};
    case ALBUMS_OF_ARTIST_FAILURE:
      return {...state, error: action.error};
    case ADD_NEW_ALBUM_SUCCESS:
      return {...state, albums: action.data};
    case ADD_NEW_ALBUM_FAILURE:
      return {...state, error: action.error};
    case GET_MY_ALBUMS_LIST_SUCCESS:
      return {...state, albums: action.data};
    case GET_MY_ALBUMS_LIST_FAILURE:
      return {...state, error: action.error};
    case GET_ALBUMS_LIST_FOR_ADMIN_SUCCESS:
      return {...state, albums: action.albums};
    case GET_ALBUMS_LIST_FOR_ADMIN_FAILURE:
      return {...state, error: action.error};

    default:
      return state;
  }
};

export default reducer;