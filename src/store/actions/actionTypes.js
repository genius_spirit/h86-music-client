export const ARTIST_LIST_SUCCESS = 'ARTIST_LIST_SUCCESS';
export const ARTIST_LIST_FAILURE = 'ARTIST_LIST_FAILURE';

export const GET_ARTIST_LIST_FOR_ADMIN_SUCCESS = 'GET_ARTIST_LIST_FOR_ADMIN_SUCCESS';
export const GET_ARTIST_LIST_FOR_ADMIN_FAILURE = 'GET_ARTIST_LIST_FOR_ADMIN_FAILURE';

export const ALBUMS_OF_ARTIST_SUCCESS = 'ALBUMS_OF_ARTIST_SUCCESS';
export const ALBUMS_OF_ARTIST_FAILURE = 'ALBUMS_OF_ARTIST_FAILURE';

export const ALBUMS_LIST_SUCCESS = 'ALBUMS_LIST_SUCCESS';
export const ALBUMS_LIST_FAILURE = 'ALBUMS_LIST_FAILURE';

export const GET_ALBUMS_LIST_FOR_ADMIN_SUCCESS = 'GET_ALBUMS_LIST_FOR_ADMIN_SUCCESS';
export const GET_ALBUMS_LIST_FOR_ADMIN_FAILURE = 'GET_ALBUMS_LIST_FOR_ADMIN_FAILURE';

export const TRACKS_OF_ALBUM_SUCCESS = 'TRACKS_OF_ALBUM_SUCCESS';
export const TRACKS_OF_ALBUM_FAILURE = 'TRACKS_OF_ALBUM_FAILURE';

export const GET_ALBUM_BY_ID_SUCCESS = 'GET_ALBUM_BY_ID_SUCCESS';
export const GET_ALBUM_BY_ID_FAILURE = 'GET_ALBUM_BY_ID_FAILURE';

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const POST_TRACK_TO_HISTORY_SUCCESS = 'POST_TRACK_TO_HISTORY_SUCCESS';
export const POST_TRACK_TO_HISTORY_FAILURE = 'POST_TRACK_TO_HISTORY_FAILURE';

export const TRACKS_HISTORY_SUCCESS = 'TRACKS_HISTORY_SUCCESS';
export const TRACKS_HISTORY_FAILURE = 'TRACKS_HISTORY_FAILURE';

export const LOGOUT_USER = 'LOGOUT_USER';

export const ADD_NEW_ARTIST_SUCCESS = 'ADD_NEW_ARTIST_SUCCESS';
export const ADD_NEW_ARTIST_FAILURE = 'ADD_NEW_ARTIST_FAILURE';

export const GET_MY_ARTISTS_LIST_SUCCESS = 'GET_MY_ARTISTS_LIST_SUCCESS';
export const GET_MY_ARTISTS_LIST_FAILURE = 'GET_MY_ARTISTS_LIST_FAILURE';

export const ADD_NEW_ALBUM_SUCCESS = 'ADD_NEW_ALBUM_SUCCESS';
export const ADD_NEW_ALBUM_FAILURE = 'ADD_NEW_ALBUM_FAILURE';

export const GET_MY_ALBUMS_LIST_SUCCESS = 'GET_MY_ALBUMS_LIST_SUCCESS';
export const GET_MY_ALBUMS_LIST_FAILURE = 'GET_MY_ALBUMS_LIST_FAILURE';

export const ADD_NEW_TRACK_SUCCESS = 'ADD_NEW_TRACK_SUCCESS';
export const ADD_NEW_TRACK_FAILURE = 'ADD_NEW_TRACK_FAILURE';