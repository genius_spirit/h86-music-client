import axios from "../../axios-api";
import { push } from "react-router-redux";
import {
  ADD_NEW_TRACK_FAILURE,
  ADD_NEW_TRACK_SUCCESS,
  GET_ALBUM_BY_ID_FAILURE,
  GET_ALBUM_BY_ID_SUCCESS,
  TRACKS_OF_ALBUM_FAILURE,
  TRACKS_OF_ALBUM_SUCCESS
} from "./actionTypes";

const tracksOfAlbumSuccess = tracksData => {
  return {type: TRACKS_OF_ALBUM_SUCCESS, tracksData};
};

const tracksOfAlbumFailure = error => {
  return {type: TRACKS_OF_ALBUM_FAILURE, error};
};

export const getTracksOfAlbum = (id) => {
  return (dispatch, getState) => {
    const headers = { 'Token': getState().users.user.token };
    axios.get(`/tracks?album=${id}`, {headers}).then(
      response => {
        dispatch(tracksOfAlbumSuccess(response.data));
        dispatch(push(`/tracks?album=${id}`));
        dispatch(getAlbumById(id));
      },
      error => {tracksOfAlbumFailure(error)}
    );
  };
};

const getAlbumByIdSuccess = albumData => {
  return {type: GET_ALBUM_BY_ID_SUCCESS, albumData};
};

const getAlbumByIdFailure = error => {
  return {type: GET_ALBUM_BY_ID_FAILURE, error};
};

export const getAlbumById = (id) => {
  return dispatch => {
    axios.get(`/albums/${id}`).then(
      response => {
        dispatch(getAlbumByIdSuccess(response.data));
      },
      error => {
        dispatch(getAlbumByIdFailure(error));
      }
    );
  };
};

const addNewTrackSuccess = data => {
  return { type: ADD_NEW_TRACK_SUCCESS, data };
};

const addNewTrackFailure = error => {
  return { type: ADD_NEW_TRACK_FAILURE, error };
};

export const addNewTrack = trackData => {
  return (dispatch, getState) => {
    const headers = {
      Token: getState().users.user.token
    };
    return axios.post("/tracks", trackData, { headers }).then(
      response => {
        dispatch(addNewTrackSuccess(response.data));
        dispatch(push("/"));
      },
      error => {
        dispatch(addNewTrackFailure(error));
      }
    );
  };
};