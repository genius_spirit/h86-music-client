import axios from "../../axios-api";
import { push } from "react-router-redux";

import {
  ALBUMS_OF_ARTIST_FAILURE,
  ALBUMS_OF_ARTIST_SUCCESS,
  ALBUMS_LIST_SUCCESS,
  ADD_NEW_ALBUM_SUCCESS,
  ADD_NEW_ALBUM_FAILURE,
  GET_MY_ALBUMS_LIST_SUCCESS,
  GET_MY_ALBUMS_LIST_FAILURE,
  ALBUMS_LIST_FAILURE,
  GET_ALBUMS_LIST_FOR_ADMIN_SUCCESS,
  GET_ALBUMS_LIST_FOR_ADMIN_FAILURE
} from "./actionTypes";

const AlbumsOfArtistSuccess = data => {
  return { type: ALBUMS_OF_ARTIST_SUCCESS, data };
};

const AlbumsOfArtistFailure = error => {
  return { type: ALBUMS_OF_ARTIST_FAILURE, error };
};

export const getAlbumsOfArtist = id => {
  return (dispatch, getState) => {
    const headers = { 'Token': getState().users.user.token };
    axios.get(`/albums?artist=${id}`, {headers}).then(
      response => {
        dispatch(AlbumsOfArtistSuccess(response.data));
        dispatch(push(`/albums?artist=${id}`));
      },
      error => {
        dispatch(AlbumsOfArtistFailure(error));
      }
    );
  };
};

const getAlbumsListSuccess = data => {
  return { type: ALBUMS_LIST_SUCCESS, data };
};

const getAlbumsListFailure = error => {
  return { type: ALBUMS_LIST_FAILURE, error };
};

export const getAlbumsList = () => {
  return (dispatch) => {
    axios.get("/albums").then(response => {
      dispatch(getAlbumsListSuccess(response.data));
    }, error => dispatch(getAlbumsListFailure(error)));
  };
};

const getAlbumsListForAdminSuccess = albums => {
  return { type: GET_ALBUMS_LIST_FOR_ADMIN_SUCCESS, albums };
};

const getAlbumsListForAdminFailure = error => {
  return { type: GET_ALBUMS_LIST_FOR_ADMIN_FAILURE, error };
};

export const getAlbumsListForAdmin = () => {
  return (dispatch, getState) => {
    const headers = { Token: getState().users.user.token };
    return axios.get("/albums/forAdmin", {headers}).then(
      response => {
        dispatch(getAlbumsListForAdminSuccess(response.data));
      },
      error => {
        dispatch(getAlbumsListForAdminFailure(error));
      }
    );
  };
};

const addNewAlbumSuccess = data => {
  return { type: ADD_NEW_ALBUM_SUCCESS, data };
};

const addNewAlbumFailure = error => {
  return { type: ADD_NEW_ALBUM_FAILURE, error };
};

export const addNewAlbum = albumData => {
  return (dispatch, getState) => {
    const headers = {
      Token: getState().users.user.token
    };
    return axios.post("/albums", albumData, { headers }).then(
      response => {
        dispatch(addNewAlbumSuccess(response.data));
        dispatch(push("/"));
      },
      error => {
        dispatch(addNewAlbumFailure(error));
      }
    );
  };
};

const getMyAlbumsListSuccess = data => {
  return { type: GET_MY_ALBUMS_LIST_SUCCESS, data };
};

const getMyAlbumsListFailure = error => {
  return { type: GET_MY_ALBUMS_LIST_FAILURE, error };
};

export const getMyAlbumsList = userId => (dispatch) => {
  return axios.get(`/albums/${userId}`).then(
    response => {
      dispatch(getMyAlbumsListSuccess(response.data))
    },
    error => {
      dispatch(getMyAlbumsListFailure(error))
    }
  );
};
