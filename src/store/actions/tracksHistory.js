import axios from "../../axios-api";
import {
  POST_TRACK_TO_HISTORY_FAILURE,
  TRACKS_HISTORY_FAILURE,
  TRACKS_HISTORY_SUCCESS
} from "./actionTypes";

const postTrackToHistoryFailure = error => {
  return {type: POST_TRACK_TO_HISTORY_FAILURE, error};
};

export const postTrackToHistory = (track, token) => {
  return dispatch => {
    axios.post('/tracks_history', {track: track}, {headers: {'Token': token}} ).then(
      response => {},
      error => {
        dispatch(postTrackToHistoryFailure(error))
      }
    );
  };
};

const tracksHistorySuccess = history => {
  return {type: TRACKS_HISTORY_SUCCESS, history};
};

const tracksHistoryFailure = error => {
  return {type: TRACKS_HISTORY_FAILURE, error};
};

export const getTracksHistory = id => {
  return dispatch => {
    axios.get(`/tracks_history/${id}`).then(
      response => {
        dispatch(tracksHistorySuccess(response.data))
      },
      error => {
        dispatch(tracksHistoryFailure(error))
      }
    );
  };
};