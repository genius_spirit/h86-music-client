import axios from "../../axios-api";
import { push } from "react-router-redux";
import {
  ADD_NEW_ARTIST_FAILURE,
  ADD_NEW_ARTIST_SUCCESS,
  ARTIST_LIST_FAILURE,
  ARTIST_LIST_SUCCESS,
  GET_ARTIST_LIST_FOR_ADMIN_FAILURE,
  GET_ARTIST_LIST_FOR_ADMIN_SUCCESS,
  GET_MY_ARTISTS_LIST_FAILURE,
  GET_MY_ARTISTS_LIST_SUCCESS
} from "./actionTypes";

const fetchArtistsListSuccess = data => {
  return { type: ARTIST_LIST_SUCCESS, data };
};

const fetchArtistListFailure = error => {
  return { type: ARTIST_LIST_FAILURE, error };
};

export const fetchArtistsList = () => {
  return dispatch => {
    return axios.get("/artists").then(
      response => {
        dispatch(fetchArtistsListSuccess(response.data));
      },
      error => {
        dispatch(fetchArtistListFailure(error));
      }
    );
  };
};

const getArtistsListForAdminSuccess = data => {
  return { type: GET_ARTIST_LIST_FOR_ADMIN_SUCCESS, data };
};

const getArtistListForAdminFailure = error => {
  return { type: GET_ARTIST_LIST_FOR_ADMIN_FAILURE, error };
};

export const getArtistsListForAdmin = () => {
  return (dispatch, getState) => {
    const headers = { Token: getState().users.user.token };
    return axios.get("/artists/admin", {headers}).then(
      response => {
        dispatch(getArtistsListForAdminSuccess(response.data));
        dispatch(push('/artists/admin'));
      },
      error => {
        dispatch(getArtistListForAdminFailure(error));
      }
    );
  };
};

const addNewArtistSuccess = data => {
  return { type: ADD_NEW_ARTIST_SUCCESS, data };
};

const addNewArtistFailure = error => {
  return { type: ADD_NEW_ARTIST_FAILURE, error };
};

export const addNewArtist = artistData => {
  return (dispatch, getState) => {
    const headers = {
      Token: getState().users.user.token
    };
    return axios.post("/artists", artistData, { headers }).then(
      response => {
        dispatch(addNewArtistSuccess(response.data));
        dispatch(push("/"));
      },
      error => {
        dispatch(addNewArtistFailure(error));
      }
    );
  };
};

const getMyArtistsListSuccess = data => {
  return { type: GET_MY_ARTISTS_LIST_SUCCESS, data };
};

const getMyArtistsListFailure = error => {
  return { type: GET_MY_ARTISTS_LIST_FAILURE, error };
};

export const getMyArtistsList = userId => (dispatch, getState) => {
  const headers = { Token: getState().users.user.token };
  return axios.get(`/artists/${userId}`, { headers }).then(
    response => {
      dispatch(getMyArtistsListSuccess(response.data));
    },
    error => {
      dispatch(getMyArtistsListFailure(error));
    }
  );
};
