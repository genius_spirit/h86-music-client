import { routerMiddleware, routerReducer } from "react-router-redux";
import thunkMiddleware from "redux-thunk";
import createHistory from "history/createBrowserHistory";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import albumsReducer from "./reducers/albums";
import artistsReducer from "./reducers/artists";
import tracksReducer from "./reducers/tracks";
import usersReducer from "./reducers/users";
import tracksHistoryReducer from "./reducers/tracksHistory";
import { readState, saveState } from "./localStorage";

const rootReducer = combineReducers({
  artists: artistsReducer,
  albums: albumsReducer,
  tracks: tracksReducer,
  users: usersReducer,
  tracksHistory: tracksHistoryReducer,
  routing: routerReducer
});

export const history = createHistory();

const middleware = [
  thunkMiddleware,
  routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(applyMiddleware(...middleware));

const persistedState = readState();

const store = createStore(rootReducer, persistedState, enhancer);

store.subscribe(() => {
  saveState({users: {user: store.getState().users.user}});
});

export default store;